/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DOTA_CHAT.Interfaces;

import java.beans.PropertyChangeListener;

/**
 *
 * @author Maras
 */
public interface IUzytkownik {
    
    int getPort();
    void setPort(int port);
    
    String getAdresIP();
    void setAdresIP(String adresIP);
    
    String getHaslo();
    void setHaslo(String haslo);
    
    String getLogin();
    void setLogin(String login);
   
    void addPropertyChangeListener(PropertyChangeListener listener);
    void removePropertyChangeListener(PropertyChangeListener listener);
}
