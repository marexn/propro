/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DOTA_CHAT.Impl;

/**
 *
 * @author Maras
 */
/**
 * Nakov Chat Client
 * (c) Svetlin Nakov, 2002
 * http://www.nakov.com
 *
 * NakovChatClient connects to Nakov Chat Server and prints all the messages
 * received from the server. It also allows the user to send messages to the
 * server. NakovChatClient thread reads messages and print them to the standard
 * output. Sender thread reads messages from the standard input and sends them
 * to the server.
 */
 

import java.io.*;
import java.net.*;
 
public class ChatClient
{
    public static final String SERVER_HOSTNAME = "localhost";
    public static final int SERVER_PORT = 1728;
    
    
    public static void main(String[] args)
    {
       
        
        BufferedReader in = null;
        PrintWriter out = null;
        try {
           // Connect to DOTA_Chat Server
           Socket socket = new Socket(SERVER_HOSTNAME, SERVER_PORT);
           in = new BufferedReader(
               new InputStreamReader(socket.getInputStream()));
           out = new PrintWriter(
               new OutputStreamWriter(socket.getOutputStream()));
           System.out.println("Connected to server " +
              SERVER_HOSTNAME + ":" + SERVER_PORT);
        } catch (IOException ioe) {
           System.err.println("Can not establish connection to " +
               SERVER_HOSTNAME + ":" + SERVER_PORT);
           ioe.printStackTrace();
           System.exit(-1);
        }
 
        // Create and start Sender thread
        Sender sender = new Sender(out);
        sender.setDaemon(true);
        sender.start();
 
        try {
           // Read messages from the server and print them
            String message;
           
          while ((message=in.readLine()) != null) {
               System.out.println(message);
           }
        } catch (IOException ioe) {
           System.err.println("Connection to server broken.");
           ioe.printStackTrace();
        }
 
    }
}
 
