/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mailsender.impl;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.apache.commons.lang3.StringUtils;


/**
 *
 * @author Zofia
 */
@Component("mailComponent")
public class SendEmailImpl implements com.mailsender.contract.SendEmail {
    
    @Autowired
    private JavaMailSender mailSender;
    
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }
    
    
    @Override
    public void sendMail(final String from, final String to,
                          final String subject, final String text)
    {
        sendMailAttachment(from, to, subject, text, null, null);
 
    }
    
    @Override
    public void sendMailAttachment(final String from, final String to, final String subject, final String text,
                                final String attachmentPath, final String attachmentName)
    {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
 
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
 
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
                message.setTo(to);
                message.setFrom(new InternetAddress(from));
                message.setSubject(subject);
                message.setText(text, true);
 
                if (!StringUtils.isBlank(attachmentPath)) {
                    FileSystemResource file = new FileSystemResource(attachmentPath);
                    message.addAttachment(attachmentName, file);
                }
            }
        };
        this.mailSender.send(preparator);
}
}
