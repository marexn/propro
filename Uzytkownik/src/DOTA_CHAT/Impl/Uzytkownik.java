/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DOTA_CHAT.Impl;

import DOTA_CHAT.Interfaces.IUzytkownik;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author Maras
 */
public class Uzytkownik implements IUzytkownik{
   /* private String login;
    private String haslo;
    private String adresIP;
    private int port; */
    
    private String login;
    private String haslo;
    private String adresIP;
    private int port;

    

    
    
    public static final String PROP_PORT = "port";

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        int oldPort = this.port;
        this.port = port;
        propertyChangeSupport.firePropertyChange(PROP_PORT, oldPort, port);
    }


    public static final String PROP_ADRESIP = "adresIP";

    public String getAdresIP() {
        return adresIP;
    }

    public void setAdresIP(String adresIP) {
        String oldAdresIP = this.adresIP;
        this.adresIP = adresIP;
        propertyChangeSupport.firePropertyChange(PROP_ADRESIP, oldAdresIP, adresIP);
    }


    public static final String PROP_HASLO = "haslo";

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        String oldHaslo = this.haslo;
        this.haslo = haslo;
        propertyChangeSupport.firePropertyChange(PROP_HASLO, oldHaslo, haslo);
    }


    public static final String PROP_LOGIN = "login";

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        String oldLogin = this.login;
        this.login = login;
        propertyChangeSupport.firePropertyChange(PROP_LOGIN, oldLogin, login);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }



    
    
}
