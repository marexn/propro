/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DOTA_CHAT.Impl;

//import DOTA_CHAT.Interfaces.IRejestracja;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Scanner;
import javax.swing.JOptionPane;

//import DOTA_CHAT.Interfaces.IUzytkownik;

/**
 *
 * @author Maras
 */
public class RejestracjaUzytkownika{
    //IUzytkownik uzytkownik;

    //public RejestracjaUzytkownika(IUzytkownik uzytkownik) {
        //this.uzytkownik = uzytkownik;
    //}
     private static final int[] chain = {1941, 12394, 23093, 9023, 2111, 193, 93, 293};

    public static String encrypt(String key) {
        String result = "";
        int l = key.length();
        char ch;
        int ck = 0;
        for (int i = 0; i < l; i++) {
            if (ck >= chain.length - 1) {
                ck = 0;
            }
            ch = key.charAt(i);
            ch += chain[ck];
            result += ch;
            ck++;

        }
        return result;
    }
    
    
     
    public void Rejestracja(String userBinding, String passwordBinding){
        
        String st = "Rejestracja przebiegła pomyślnie, możesz się zalogować.";
        String path = ".\\BazaUzytkownikow.txt";

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path, true));
            String user = userBinding;
            String password = encrypt(passwordBinding);
            File f = new File(".\\BazaUzytkownikow.txt");
            if (f.exists()) {

                LineNumberReader lnr = new LineNumberReader(new FileReader(path));
                lnr.skip(Long.MAX_VALUE);
                int rows = lnr.getLineNumber();
                if (rows > 0) {

                    Scanner scanner = new Scanner(f);

                    int lineNum = 0;
                    boolean checkState = false;
                    while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                        lineNum++;
                        if (line.matches(user)) {
                            JOptionPane.showMessageDialog(null, "Ta nazwa użytkownika jest już zajęta. Spróbuj ponownie.");
                            checkState = true;
                            break;
                            
                        } 
                        
                    }
                    if (checkState == false){
                            writer.newLine();
                            writer.write(user);
                            writer.newLine();
                            writer.write(password);
                            writer.close();
                            JOptionPane.showMessageDialog(null, st);
                            
                        }else if (checkState == true){
                          
                        }
                } else {
                    writer.write(user);
                    writer.newLine();
                    writer.write(password);
                    writer.close();
                    JOptionPane.showMessageDialog(null, st);
                }
            } else {
                writer.write(user);
                writer.newLine();
                writer.write(password);
                writer.close();
                JOptionPane.showMessageDialog(null, st);
            }
   

        } catch (IOException e) {
            e.printStackTrace();
        }


    }  
    
}
