/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DOTA_CHAT.Impl;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Maras
 */
public class LogowanieUzytkownik  {
   /* private String nick;
    private String haslo;

    public LogowanieUzytkownik(String nick, String haslo) {
        this.nick = nick;
        this.haslo = haslo;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }
    */
    
     private static final int[] chain = {1941, 12394, 23093, 9023, 2111, 193, 93, 293};

   
    public static String decrypt(String key) {
        String result = "";
        int l = key.length();
        char ch;
        int ck = 0;
        for (int i = 0; i < l; i++) {
            if (ck >= chain.length - 1) {
                ck = 0;
            }
            ch = key.charAt(i);
            ch -= chain[ck];
            result += ch;
            ck++;

        }
        return result;
    }

  
      
public void Logowanie(String userBinding, String passwordBinding){
        File file = new File(".\\BazaUzytkownikow.txt");

        try {
            Scanner scanner = new Scanner(file);
            String user = userBinding;
            String password = passwordBinding;
           
            int lineNum = 0;
            boolean checkState = false;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lineNum++;
                if (line.matches(user)) {
                    checkState = true;
                    String passwordLine = scanner.nextLine();
                    String correctPassword = decrypt(passwordLine);
                    lineNum++;
                    if (correctPassword.matches(password)) {
                        JOptionPane.showMessageDialog(null, userBinding+" zosta(ł/ła) zalogowan(y/a)!");
                    break;
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Nieprawidłowe hasło. Spróbuj ponownie.");
                        break;
                }
                } 
            }
            if (checkState == false) {
               JOptionPane.showMessageDialog(null, "W bazie nie ma użytkownika: " + userBinding + ". Zarejestruj się!"); 
            }
        } catch (FileNotFoundException e) {
    
        }
    }                                  
    
}
