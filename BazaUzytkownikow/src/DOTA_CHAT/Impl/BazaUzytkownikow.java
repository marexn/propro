/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DOTA_CHAT.Impl;

import DOTA_CHAT.Interfaces.IBazaUzytkownikow;
import DOTA_CHAT.Interfaces.IUzytkownik;

/**
 *
 * @author Maras
 */
public class BazaUzytkownikow implements IBazaUzytkownikow{
    private IUzytkownik[] listaUzytkownikow;

    public BazaUzytkownikow(IUzytkownik[] listaUzytkownikow) {
        this.listaUzytkownikow = listaUzytkownikow;
    }

    public IUzytkownik[] getListaUzytkownikow() {
        return listaUzytkownikow;
    }

    public void setListaUzytkownikow(IUzytkownik[] listaUzytkownikow) {
        this.listaUzytkownikow = listaUzytkownikow;
    }

   
}
